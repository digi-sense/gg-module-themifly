module bitbucket.org/digi-sense/gg-module-themifly

go 1.18

require (
	bitbucket.org/digi-sense/gg-core v0.1.93
	bitbucket.org/digi-sense/gg-core-x v0.1.93
	github.com/cbroglie/mustache v1.3.1
	golang.org/x/net v0.0.0-20220706163947-c90051bbdb60
)

require (
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/arangodb/go-driver v1.3.2 // indirect
	github.com/arangodb/go-velocypack v0.0.0-20200318135517-5af53c29c67e // indirect
	github.com/denisenkom/go-mssqldb v0.12.2 // indirect
	github.com/dlclark/regexp2 v1.4.1-0.20201116162257-a2a8dda75c91 // indirect
	github.com/dop251/goja v0.0.0-20220705101429-189bfeb9f530 // indirect
	github.com/fasthttp/websocket v1.5.0 // indirect
	github.com/fogleman/gg v1.3.0 // indirect
	github.com/go-sourcemap/sourcemap v2.1.3+incompatible // indirect
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/gofiber/fiber/v2 v2.35.0 // indirect
	github.com/gofiber/websocket/v2 v2.0.23 // indirect
	github.com/golang-sql/civil v0.0.0-20220223132316-b832511892a9 // indirect
	github.com/golang-sql/sqlexp v0.1.0 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/jackc/chunkreader/v2 v2.0.1 // indirect
	github.com/jackc/pgconn v1.12.1 // indirect
	github.com/jackc/pgio v1.0.0 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgproto3/v2 v2.3.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20200714003250-2b9c44734f2b // indirect
	github.com/jackc/pgtype v1.11.0 // indirect
	github.com/jackc/pgx/v4 v4.16.1 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/jlaffaye/ftp v0.0.0-20220630165035-11536801d1ff // indirect
	github.com/klauspost/compress v1.15.7 // indirect
	github.com/kr/fs v0.1.0 // indirect
	github.com/makiuchi-d/gozxing v0.1.1 // indirect
	github.com/mattn/go-sqlite3 v1.14.14 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pkg/sftp v1.13.5 // indirect
	github.com/savsgio/gotils v0.0.0-20220530130905-52f3993e8d6d // indirect
	github.com/streadway/amqp v1.0.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.38.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	github.com/yeqown/go-qrcode v1.5.10 // indirect
	github.com/yeqown/reedsolomon v1.0.0 // indirect
	go.etcd.io/bbolt v1.3.6 // indirect
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d // indirect
	golang.org/x/image v0.0.0-20220617043117-41969df76e82 // indirect
	golang.org/x/sys v0.0.0-20220708085239-5a0f0661e09d // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/xerrors v0.0.0-20220609144429-65e65417b02f // indirect
	gorm.io/driver/mysql v1.3.4 // indirect
	gorm.io/driver/postgres v1.3.8 // indirect
	gorm.io/driver/sqlite v1.3.6 // indirect
	gorm.io/driver/sqlserver v1.3.2 // indirect
	gorm.io/gorm v1.23.8 // indirect
)
