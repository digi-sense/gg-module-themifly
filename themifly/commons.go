package themifly

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs/vfscommons"
	"errors"
	"github.com/cbroglie/mustache"
	"golang.org/x/net/html"
	"strings"
)

const (
	Name    = "ThemiFly"
	Version = "0.1.1"

	SelectorThemiflyWidgetBlogPostHere          = "themifly-widget-blog-posts-here"
	SelectorThemiflyWidgetBlogAuthorHere        = "themifly-widget-blog-author-here"
	SelectorThemiflyWidgetBlogTagsAllHere       = "themifly-widget-blog-tags-all-here"
	SelectorThemiflyWidgetBlogCategoriesHere    = "themifly-widget-blog-categories-here"
	SelectorThemiflyWidgetBlogCategoriesAllHere = "themifly-widget-blog-categories-all-here"
	SelectorThemiflyWidgetBlogPrevNextHere      = "themifly-widget-blog-prev-next-here"
	SelectorThemiflyWidgetBlogPostContentHere   = "themifly-widget-blog-post-content-here"

	BlockTypeHTML     = "html"   // simple text to replace
	BlockTypeWidget   = "widget" // dynamic widget
	BlockTypeMustache = "mustache"

	FlagNoOverwrite = "no_overwrite"
)

var (
	ErrorMissingWidgetSelector   = errors.New("missing_widget_selector")
	ErrorMissingBlogTemplate     = errors.New("missing_blog_template")
	ErrorMissingBlogPostTemplate = errors.New("missing_blog_post_template")
	ErrorMissingBlock            = errors.New("missing_block")
	ErrorFileNotFound            = errors.New("file_not_found")
	ErrorUnableToAppendNode      = errors.New("unable_to_append_node")
)

func isHTML(text string) bool {
	return strings.Index(text, "<") > -1 && strings.Index(text, "/") > 0
}

func isBase64Image(text string) bool {
	return strings.HasPrefix(text, "data:image")
}

func relativizePath(path string) string {
	if !strings.HasPrefix(path, "./") {
		return "./" + path
	}
	return path
}

func vfs(absRoot string, settings *ThemiflySettingsTarget) (vfscommons.IVfs, error) {
	v, err := ggx.VFS.New(settings)
	if nil != err {
		return nil, err
	}
	if len(settings.Home) > 0 {
		_, err = v.Cd(settings.Home)
		if nil != err {
			return nil, err
		}
	}

	scheme, host := settings.SplitLocation()
	if scheme == vfscommons.SchemaOS && !gg.Paths.IsAbs(host) {
		_, err = v.Cd(gg.Paths.Concat(absRoot, host))
	}

	return v, err
}

func vfsSave(vfs vfscommons.IVfs, doc *html.Node, pageName string) (err error) {
	parser := NewHtmlParser()
	pageHTML := parser.RenderNode(doc)
	_, err = vfs.Write([]byte(pageHTML), pageName)
	return
}

func vfsSaveImage(vfs vfscommons.IVfs, text string) (uri string, err error) {
	if isBase64Image(text) {
		base := strings.Replace(text, "data:image/jpeg;base64,", "", 1)
		base = strings.Replace(base, "data:image/png;base64,", "", 1)
		// decode
		bytes, decError := gg.Coding.DecodeBase64(strings.TrimSpace(base))
		if nil != decError {
			return "", decError
		}

		name := gg.Coding.MD5(base)
		uri = "images/" + name + ".png"
		_, err = vfs.Write(bytes, "./"+uri)
	}
	return
}

func vfsRead(vfs vfscommons.IVfs, pageName string) (string, error) {
	if b, _ := vfs.Exists(pageName); b {
		data, err := vfs.Read(pageName)
		if nil != err {
			return "", err
		}
		return string(data), nil
	}
	return "", gg.Errors.Prefix(ErrorFileNotFound, pageName)
}

func vfsReadOrCreate(vfs vfscommons.IVfs, pageName string, defContent string) (string, error) {
	if b, _ := vfs.Exists(pageName); b {
		data, err := vfs.Read(pageName)
		if nil != err {
			return "", err
		}
		return string(data), nil
	} else {
		_, err := vfs.Write([]byte(defContent), pageName)
		if nil != err {
			return "", err
		}
		return defContent, err
	}
}

func merge(params ...interface{}) map[string]interface{} {
	response := make(map[string]interface{})
	for _, param := range params {
		m := gg.Convert.ToMap(param)
		if nil != m {
			gg.Maps.Merge(false, response, m)
		}
	}
	return response
}

func renderDocument(parser *HtmlParser, pageRawHtml string, blocks []*BlockItem, data map[string]interface{}) (*html.Node, error) {
	// replace blocks and render HTML
	renderedHTML, _, err := renderBlocks(pageRawHtml, blocks, data)
	if nil != err {
		return nil, err
	}

	// create document
	doc, err := parser.WrapText(renderedHTML)
	if nil != err {
		return nil, err
	}
	if nil == doc {
		return nil, ErrorMissingBlogTemplate
	}

	return doc, nil
}

func renderBlocks(pageRawHtml string, blocks []*BlockItem, data map[string]interface{}) (renderedHTML string, replaced map[string]interface{}, err error) {
	// get data from mustache blocks
	blocksData := getBlocksMustacheData(blocks)

	// pre-rendering to avoid broken HTML
	renderedHTML, err = render(pageRawHtml, data, blocksData)

	// replace existing blocks (HTML, WIDGETS)
	renderedHTML, replaced = replaceBlocks(renderedHTML, blocks)

	// render HTML
	renderedHTML, err = render(renderedHTML, data, blocksData)
	if nil != err {
		return "", nil, err
	}
	return
}

func render(template string, model ...interface{}) (string, error) {
	return mustache.Render(template, model...)
}

func replaceBlocks(text string, blocks []*BlockItem) (string, map[string]interface{}) {
	replaced := map[string]interface{}{}
	for _, block := range blocks {
		if len(block.Match) > 0 && len(block.Replace) > 0 {
			switch block.Type {
			case BlockTypeHTML:
				// replace
				t, count := replaceText(text, block.Match, block.Replace)
				if count > 0 {
					text = t
				}
				replaced[block.Name] = count
			case BlockTypeWidget:
				// append
				t, b := replaceWidget(text, block.Match, block.Replace, block.Flags)
				if b && len(t) > 0 {
					text = t
					replaced[block.Name] = 1
				} else {
					replaced[block.Name] = 0
				}
			}
		}
	}
	return text, replaced
}

func replaceWidget(text, selector, childHTML string, flags []string) (string, bool) {
	parser := NewHtmlParser()
	doc, err := parser.WrapText(text)
	if nil != err || nil == doc {
		return "", false
	}
	found := false
	parser.ForEach(doc, func(node *html.Node) bool {
		if parser.Matches(node, selector) {
			// found
			found = true
			noOverwrite := gg.Arrays.IndexOf(FlagNoOverwrite, flags) > -1
			hasChildren := parser.CountChildrenNoText(node) > 0
			canClean := !noOverwrite && hasChildren
			canAppend := !noOverwrite || (noOverwrite && !hasChildren)
			if canClean {
				// remove children
				parser.RemoveChildren(node)
			}
			if canAppend {
				parser.AppendChildText(childHTML, node)
			}

		}
		return false // next node
	})
	if found {
		return parser.RenderNode(doc), true
	}

	return "", false
}

func replaceText(text, old, new string) (string, int) {
	count := 0
	if strings.Index(text, old) > -1 {
		// RAW TEXT REPLACEMENT
		for {
			if strings.Index(text, old) == -1 {
				break
			}
			count++
			text = strings.Replace(text, old, new, 1)
		}
		return text, 1
	}

	parser := NewHtmlParser()
	doc, err := parser.WrapText(text)
	if nil == err {
		nOld := parser.TryWrapTextNode(old)
		if nil != nOld {
			parser.ForEach(doc, func(node *html.Node) bool {
				clean := gg.Strings.Slugify(parser.RenderNode(node), "")
				match := gg.Strings.Slugify(parser.RenderNode(nOld), "")
				if clean == match {
					// found match
					nNew := parser.TryWrapTextNode(new)
					if nil != nNew {
						count++
						node.Parent.InsertBefore(nNew, node)
						node.Parent.RemoveChild(node)
					}
				}
				return false // continue
			})
			if count > 0 {
				text = parser.RenderNode(doc)
			}
		}
	}

	return text, count
}

func readMatchReplacePair(dirBlocks, dir string) (match, replace string, err error) {
	match, err = gg.IO.ReadTextFromFile(gg.Paths.Concat(dirBlocks, dir, "match.html"))
	if nil != err {
		match, err = gg.IO.ReadTextFromFile(gg.Paths.Concat(dirBlocks, dir, "match.txt"))
		if nil != err {
			match = "" // match is
		}
	}
	replace, err = gg.IO.ReadTextFromFile(gg.Paths.Concat(dirBlocks, dir, "replace.html"))
	if nil != err {
		return
	}
	return
}

func getBlockWidget(blocks []*BlockItem, uid string) (*BlockItem, error) {
	for _, block := range blocks {
		if block.WidgetUID == uid {
			return block, nil
		}
	}
	return nil, gg.Errors.Prefix(ErrorMissingBlock, uid)
}

func getBlocksMustacheData(blocks []*BlockItem) map[string]interface{} {
	response := make(map[string]interface{})
	for _, block := range blocks {
		if block.Type == BlockTypeMustache {
			response[block.Match] = block.Replace
		}
	}
	return response
}

func group(files []string) map[string]map[string]interface{} {
	response := make(map[string]map[string]interface{}, 0)
	for _, file := range files {
		base := gg.Paths.Dir(file)
		name := gg.Paths.FileName(file, false)
		ext := gg.Paths.ExtensionName(file)
		uid := gg.Coding.MD5(base + name)
		if _, b := response[uid]; !b {
			response[uid] = make(map[string]interface{})
		}
		m := response[uid]
		m[ext] = file
	}
	return response
}

func readFile(file string) []byte {
	data, err := gg.IO.ReadBytesFromFile(file)
	if nil == err {
		return data
	}
	return make([]byte, 0)
}
