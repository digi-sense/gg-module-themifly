package themifly

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs/vfscommons"
)

// ---------------------------------------------------------------------------------------------------------------------
//	ThemiflySettings
// ---------------------------------------------------------------------------------------------------------------------

type ThemiflySettings struct {
	TargetDir *ThemiflySettingsTarget `json:"target-dir"`
	Blog      *ThemiflyBlogSettings   `json:"blog"`
	SiteData  map[string]interface{}  `json:"site-data"`
}

type ThemiflySettingsTarget struct {
	*vfscommons.VfsSettings
	Home string `json:"home"`
}

type ThemiflyBlogSettings struct {
	PostPerPage      int  `json:"post-per-page"`
	KeepUploadedPost bool `json:"keep-uploaded-post"`
}

func LoadSettings(filename string) (*ThemiflySettings, error) {
	if b, _ := gg.Paths.Exists(filename); !b {
		_, _ = gg.IO.WriteTextToFile(SettingsJson, filename)
	}

	var instance ThemiflySettings
	// load settings
	settings, err := gg.IO.ReadTextFromFile(filename)
	if nil != err {
		return nil, err
	}
	err = gg.JSON.Read(settings, &instance)
	if nil != err {
		return nil, err
	}

	return &instance, err
}
