package themifly

import (
	"bitbucket.org/digi-sense/gg-core"
	"strings"
)

// ---------------------------------------------------------------------------------------------------------------------
//	BlockItem
// ---------------------------------------------------------------------------------------------------------------------

type BlockItem struct {
	Name      string   `json:"name"`
	WidgetUID string   `json:"widget-uid"`
	Match     string   `json:"match"`
	Replace   string   `json:"replace"`
	Type      string   `json:"type"`
	Flags     []string `json:"flags"`
}

// ---------------------------------------------------------------------------------------------------------------------
//	TemplateItem
// ---------------------------------------------------------------------------------------------------------------------

type TemplateItem struct {
	Name         string `json:"name"`
	FullPath     string `json:"full-path"`
	RelativePath string `json:"relative-path"`
	Type         string `json:"type"`
}

// ---------------------------------------------------------------------------------------------------------------------
//	Indexer
// ---------------------------------------------------------------------------------------------------------------------

type Indexer struct {
	dirBlocks   string
	dirTemplate string

	blockItems    []*BlockItem
	templateItems []*TemplateItem
}

func NewIndexer(dirTemplate, dirBlocks string) *Indexer {
	instance := new(Indexer)
	instance.dirBlocks = dirBlocks
	instance.dirTemplate = dirTemplate

	instance.blockItems = make([]*BlockItem, 0)
	instance.templateItems = make([]*TemplateItem, 0)

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	Indexer    p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Indexer) String() string {
	m := map[string]interface{}{
		"blocks":   gg.JSON.Parse(gg.JSON.Stringify(instance.blockItems)),
		"template": gg.JSON.Parse(gg.JSON.Stringify(instance.templateItems)),
	}
	return gg.JSON.Stringify(m)
}

func (instance *Indexer) Map() map[string]interface{} {
	var m map[string]interface{}
	_ = gg.JSON.Read(gg.JSON.Stringify(instance.templateItems), &m)
	return m
}

func (instance *Indexer) SaveToFile(filename string) error {
	text := instance.String()
	_, err := gg.IO.WriteTextToFile(text, filename)
	return err
}

func (instance *Indexer) Run() error {
	// blocks
	dirs, err := gg.Paths.ReadDirOnly(instance.dirBlocks)
	if nil != err {
		return err
	}
	for _, dir := range dirs {
		item, err := instance.buildBlockItem(dir)
		if nil != err {
			return err
		}
		if nil != item {
			instance.blockItems = append(instance.blockItems, item)
		}
	}

	// template
	files, err := gg.Paths.ListFiles(instance.dirTemplate, "*.*")
	if nil != err {
		return err
	}
	for _, file := range files {
		item, err := instance.buildTemplateItem(file)
		if nil != err {
			return err
		}
		if nil != item {
			instance.templateItems = append(instance.templateItems, item)
		}
	}

	return nil
}

func (instance *Indexer) GetBlockWidget(uid string) (*BlockItem, error) {
	return getBlockWidget(instance.blockItems, uid)
}

func (instance *Indexer) GetBlocksMustacheData() map[string]interface{} {
	return getBlocksMustacheData(instance.blockItems)
}

// ---------------------------------------------------------------------------------------------------------------------
//	Indexer    p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Indexer) buildTemplateItem(path string) (*TemplateItem, error) {
	item := new(TemplateItem)
	item.Name = gg.Paths.FileName(path, false)
	item.FullPath = path
	item.RelativePath = strings.ReplaceAll(path, instance.dirTemplate, "./")
	item.Type = strings.ToLower(gg.Paths.ExtensionName(path))
	return item, nil
}

func (instance *Indexer) buildBlockItem(dir string) (*BlockItem, error) {
	match, replace, err := readMatchReplacePair(instance.dirBlocks, dir)
	if nil != err {
		return nil, err
	}

	item := new(BlockItem)
	item.Name = dir
	item.Match = match
	item.Replace = replace

	if isHTML(match) {
		item.Type = BlockTypeHTML
	} else {
		if len(item.Match)==0{
			// mustache
			item.Type = BlockTypeMustache
			item.Match = dir
		} else {
			// widget
			item.Type = BlockTypeWidget
			tokens := strings.Split(match, ":")
			item.WidgetUID = tokens[0]
			item.Match = tokens[0]
			if len(tokens) > 1 {
				flags := strings.Split(tokens[1], ",")
				item.Flags = append(item.Flags, flags...)
			}
		}
	}

	return item, nil
}
