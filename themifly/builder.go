package themifly

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_vfs/vfscommons"
)

type ThemiFlyBuilder struct {
	dirWork       string
	vfs           vfscommons.IVfs
	siteData      map[string]interface{}
	blockItems    []*BlockItem
	templateItems []*TemplateItem
}

func NewThemiFlyBuilder(dirWork string, blockItems []*BlockItem, templateItems []*TemplateItem, settings *ThemiflySettings, vfs vfscommons.IVfs) *ThemiFlyBuilder {
	instance := new(ThemiFlyBuilder)
	instance.dirWork = dirWork
	instance.siteData = settings.SiteData
	instance.blockItems = blockItems
	instance.templateItems = templateItems
	instance.vfs = vfs

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	ThemiFlyBuilder    p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance ThemiFlyBuilder) Run() (map[string]interface{}, error) {
	replaced := map[string]interface{}{}

	for _, item := range instance.templateItems {
		m, err := instance.deploy(item)
		replaced[item.Name] = m
		if nil != err {
			return replaced, err
		}
	}

	return replaced, nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	ThemiFlyBuilder    p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance ThemiFlyBuilder) deploy(item *TemplateItem) (map[string]interface{}, error) {
	if nil != item {
		text, err := gg.IO.ReadTextFromFile(item.FullPath)
		if nil != err {
			return map[string]interface{}{}, err
		}

		// replace blocks
		textToWrite, replaced, renderErr := renderBlocks(text, instance.blockItems, instance.siteData)
		if nil != renderErr {
			return map[string]interface{}{}, renderErr
		}

		// ready to write to file
		_, err = instance.vfs.Write([]byte(textToWrite), item.RelativePath)
		return replaced, err
	}
	return map[string]interface{}{}, nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	S T A T I C
// ---------------------------------------------------------------------------------------------------------------------
