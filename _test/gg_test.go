package _test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-module-themifly/themifly"
	"fmt"
	"testing"
)

func TestGGBuild(t *testing.T) {
	// create engine
	engine, err := themifly.NewThemiFly("./ggtechnologies")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// open engine
	engine.Open()

	response, err := engine.Build()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(gg.JSON.Stringify(response))
}

// Publish post
func TestGGPublish(t *testing.T) {
	// create engine
	fly, err := themifly.NewThemiFly("./ggtechnologies")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	err = fly.ResetBlogData()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// open engine
	fly.Open()

	err = fly.Publish(&themifly.ThemiflyPostMeta{
		Title:       "HELLO",
		Description: "This is a TEST POST",
		Tags:        []string{"tag3", "tag4"},
		Categories:  []string{"cat1", "cat2"},
		Author:      "Angelo",
		Date:        "20210325",
	},
		"<strong>Post</strong>",
	)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	//fmt.Println(lygo_json.Stringify(response))

	// themifly.Wait()
}
