package _test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-module-themifly/themifly"
	"fmt"
	"testing"
)


func TestAll(t *testing.T) {
	// create engine
	fly, err := themifly.NewThemiFly("./workspace")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// open engine
	fly.Open()

	response, err := fly.Build()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(gg.JSON.Stringify(response))

	// themifly.Wait()
}

func TestPublish(t *testing.T) {
	// create engine
	fly, err := themifly.NewThemiFly("./workspace")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	err = fly.ResetBlogData()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// open engine
	fly.Open()

	err = fly.Publish(&themifly.ThemiflyPostMeta{
		Title:       "HELLO",
		Description: "This is a TEST POST",
		Tags:        []string{"tag3", "tag4"},
		Categories:  []string{"cat1", "cat2"},
		Author:      "Angelo",
		Date:        "20210325",
	},
		"<strong>Post</strong>",
	)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	//fmt.Println(lygo_json.Stringify(response))

	// themifly.Wait()
}

func TestAutoPublish(t *testing.T) {
	// create engine
	fly, err := themifly.NewThemiFly("./workspace")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	err = generateStub(fly)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	_ = fly.ResetBlogData()

	// open engine
	fly.Open()

	fly.Wait()
}

func TestGeneratePostStub(t *testing.T) {
	// create engine
	fly, err := themifly.NewThemiFly("./workspace")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	err = generateStub(fly)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func generateStub(fly *themifly.ThemiFly) error {
	// create engine
	return  fly.GenerateBlogPostStub("post", "./uploads")
}
