package main

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-module-themifly/themifly"
	"flag"
	"fmt"
	"os"
)

// ---------------------------------------------------------------------------------------------------------------------
//	m a i n
// ---------------------------------------------------------------------------------------------------------------------

func main() {
	// PANIC RECOVERY
	defer func() {
		if r := recover(); r != nil {
			// recovered from panic
			message := gg.Strings.Format("[panic] APPLICATION %s ERROR: %s", themifly.Name, r)
			fmt.Println(message)
		}
	}()

	//-- command flags --//
	// build
	cmdBuild := flag.NewFlagSet("build", flag.ExitOnError)
	cmdBuildSettings := cmdBuild.String("settings", gg.Paths.Absolute("./_workspace/settings.json"), "Set file to load with required settings")
	// publish
	cmdPublish := flag.NewFlagSet("publish", flag.ExitOnError)
	cmdPublishSettings := cmdPublish.String("settings", gg.Paths.Absolute("./_workspace/settings.json"), "Set file to load with required settings")
	cmdPublishDir := cmdPublish.String("dir", gg.Paths.Absolute("./uploads"), "Set directory with post to publish")
	// stub
	cmdStub := flag.NewFlagSet("stub", flag.ExitOnError)
	cmdStubSettings := cmdStub.String("settings", gg.Paths.Absolute("./_workspace/settings.json"), "Set file to load with required settings")
	cmdStubDir := cmdStub.String("dir", "./uploads", "Set target directory")

	// parse
	if len(os.Args) > 1 {
		cmd := os.Args[1]
		switch cmd {
		case "build":
			// BUILD
			_ = cmdBuild.Parse(os.Args[2:])
			dirWork := gg.Paths.Dir(*cmdBuildSettings)
			// set paths
			gg.Paths.GetWorkspace("*").SetPath(dirWork)
			// run command
			build(*cmdBuildSettings)
		case "publish":
			// BUILD
			_ = cmdPublish.Parse(os.Args[2:])
			dirWork := gg.Paths.Dir(*cmdPublishSettings)
			// set paths
			gg.Paths.GetWorkspace("*").SetPath(dirWork)
			// run command
			publish(*cmdPublishSettings, *cmdPublishDir)
		case "stub":
			// STUB
			_ = cmdStub.Parse(os.Args[2:])
			dirWork := gg.Paths.Dir(*cmdStubSettings)
			// set paths
			gg.Paths.GetWorkspace("*").SetPath(dirWork)
			dir := *cmdStubDir
			if len(dir) == 0 {
				dir = "./uploads"
			}
			// run command
			stub(*cmdStubSettings, dir)
		}
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func build(file string) {
	themifly, err := themifly.NewThemiFly(file)
	if nil != err {
		panic(err)
	}

	// open engine
	themifly.Open()

	response, err := themifly.Build()
	if nil != err {
		panic(err)
	}

	fmt.Print(gg.Formatter.FormatMap(response))
}

func publish(file, dir string) {
	themifly, err := themifly.NewThemiFly(file)
	if nil != err {
		panic(err)
	}

	// open engine
	themifly.Open()

	err = themifly.PublishDir(dir)
	if nil != err {
		panic(err)
	}

	fmt.Print("DONE. Post published.")
}

func stub(file, target string) {
	themifly, err := themifly.NewThemiFly(file)
	if nil != err {
		panic(err)
	}

	err = themifly.GenerateBlogPostStub("new_post", target)
	if nil != err {
		panic(err)
	}

	fmt.Print(fmt.Sprintf("Stub generated into '%v'", target))
}
